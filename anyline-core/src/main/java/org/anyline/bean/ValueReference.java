package org.anyline.bean;

public interface ValueReference {
    String getName();
    void setName(String name);
}
